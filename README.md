Requirements \ Требования
------------
- Docker version 24.0.7, build afdd53b
- Ubuntu 22.04

```
Install Docker Engine on Ubuntu

curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh


Make changes to .env

docker compose build
docker compose up